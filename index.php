<?php 

function require_multi($files) {
    $files = func_get_args();
    foreach($files as $file)
        require_once($file);
}

require_multi("Animal.php", "Ape.php", "Frog.php");

$sheep = new Animal("shaun");
echo "Name : " . $sheep->name . '<br>';
echo "legs : " . $sheep->legs . '<br>';
echo "cold blooded : " . $sheep->cold_blooded . '<br><br>';

$frog = new Frog("buduk");
echo "Name : " . $frog->name . '<br>';
echo "legs : " . $frog->legs . '<br>';
echo "cold .: " . $frog->cold_blooded . '<br>';
echo $frog->jump();


$ape = new Ape("kera sakti");
echo "Name : " . $ape->name . '<br>';
echo "legs : " . $ape->legs . '<br>';
echo "cold .: " . $ape->cold_blooded . '<br>';
echo $ape->yell();

?>