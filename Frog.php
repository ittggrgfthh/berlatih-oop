<?php 


require_once "Animal.php";

class Frog extends Animal{
    public function jump(): void{
        echo "Jump : Hop Hop<br><br>";
    }
}