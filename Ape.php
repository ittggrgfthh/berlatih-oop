<?php 


require_once "Animal.php";

class Ape extends Animal{
    public int $legs = 2;

    public function yell(): void{
        echo "Yell : Auooo<br><br>";
    }
}