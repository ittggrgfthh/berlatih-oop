<?php 

class Animal{
    public string $name;
    public int $legs = 4;
    public string $cold_blooded = "no";

    public function __construct(string $animalName){
        $this->name = $animalName;
    }
}
